<?php

namespace SemiCreative\AutoForm;

class InvalidSourceException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Must provide either a table name or model as source');
    }
}
