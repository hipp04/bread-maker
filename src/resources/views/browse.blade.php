{{-- Displays browse table for BREADable resource --}}
<table class="table">
    <tr>
        @foreach (array_keys($resource->attributes) as $column)
            <th>{{ $column }}</th>
        @endforeach
    </tr>
    {{-- 
    @foreach ($resource->rows as $row)
        <tr>
            @foreach ($row as $value)
                <th>{{ $value }}</th>
            @endforeach
        </tr>
    @endforeach
     --}}
</table>